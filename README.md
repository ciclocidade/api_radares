## Para rodar os scripts no RStudio:

**Baixar o GeckoDriver**
<br>https://github.com/mozilla/geckodriver/releases

**Baixar o Selenium Standalone Server**
<br>https://selenium-release.storage.googleapis.com/index.html

**Rodar o Selenium e o GeckoDriver em um terminal**
<br>Observação: Atualizar os endereços corretos para o GeckoDriver e o Selenium no código abaixo.
<br>`java -jar -Dwebdriver.gecko.driver="/[atualizar-caminho-aqui]/geckodriver-v0.31.0-linux64/geckodriver" /[atualizar-caminho-aqui]/selenium-server-standalone-3.9.1.jar`

**Seguir os passos em Selenium Basics**
<br>Este link é o tutorial básico e os passos já estão incorporados nos scripts. Mas é uma boa referência para entender o que está acontecendo.
<br>https://docs.ropensci.org/RSelenium/articles/basics.html

## Documentação da API de Radares
Atualizado da documentação original em http://dadosradares.prefeitura.sp.gov.br.

### Método getLocais()
**Exemplo 1:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getLocais/`
<br>**Retorna:**
<br>[{"x": longitude, "y": latitude, "lote": numero_do_lote, "id": id, "codigo": [codigo(s)]}, ...]}

**Exemplo 2:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getLocais/<YYYY-MM-DD>/`
<br>**Retorna:**
<br>[{"x": longitude, "y": latitude, "lote": numero_do_lote, "id": id, "codigo": [codigo(s)]}, ...]}


### Método getDatas()
**Exemplo:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getDatas/`
<br>**Retorna:**
<br>[{"results": [dd/mm/yyyy, dd/mm/yyyy ... ]}


### Método getDetalhes()
**Exemplo:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getDetalhes/<codigo>/`
<br>**Retorna:**
<br>{"codigo": codigo, "lote": numero_do_lote, "endereco": endereco, "sentido": sentido, "referencia": referencia, "tipo_equip": tipo_de_equipamento, "enquadramento": enquadramento(s), "faixas": numero_de_faixas, "velocidade": velocidade_estipulada, "velocidade_cam_oni": velocidade_pesados, "velocidade_carro_moto": velocidade_leves, "bairro": bairro, "data_publicacao": inaugurado, "latitude": y, "longitude": x}

### Método getContagens()
**Exemplo:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getContagens/<YYYY-MM-DD>/<codigo>/`
<br>**Retorna:**
<br>{"result": [{"codigo": codigo, "faixa": faixa, "tipo": tipo_de_veiculo, "contagem": contagem, "autuacoes": autuacoes, "placas": placas, "data_e_hora": hora_da_contagem}, ... ]}

### Método getViagens()
**Exemplo:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getViagens/<YYYY-MM-DD>/<hora>/<codigo>/`
<br>**Retorna:**
<br>{"result": [{"id": viagem_id, "inicio": codigo_inicio, "final": codigo_final, "data_inicio": data_inicio, "data_final": data_final, "velocidade_media": velocidade_media(km/h)}, ...]}
<br>**Aviso:** Os cálculos de **velocidade média** e de **distância** estarão vazios na base.

### Método getTrajetos()
**Exemplo 1:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getTrajetos/<YYYY-MM-DD>/<hora>/<codigo>/`
<br>**Retorna:**
<br>{"result": [{"id": trajeto_id, "inicio": codigo_inicio, "final": codigo_final, "data_inicio": data_inicio, "data_final": data_final, "velocidade_media": velocidade_media(km/h), "distancia": (?), ..]}

**Exemplo 2:**
<br>`http://dadosradares.prefeitura.sp.gov.br/getTrajetos/<viagem_id>/`
<br>**Retorna:**
<br>{"result": [{"id": **trajeto_id**, "inicio": codigo_inicio, "final": codigo_final, "data_inicio": data_inicio, "data_final": data_final, "velocidade_media": velocidade_media(km/h), "distancia": (?), ..]}
<br>**Atenção:** na documentação da API oficial está errado: retorna **id_trajeto** em vez de **id_viagem**.
<br>**Aviso:** Os cálculos de **velocidade média** e de **distância** estarão vazios na base.


## Fluxo de trabalho para cálculo de velocidades médias

![Fluxograma API Radares](/images/Fluxo_API_Radares.png "Fluxo API Radares")
