library('tidyverse')
library('tidylog')

library('RSelenium')
library("clipr")
library('jsonlite')

# Baixar GeckoDriver:
# https://github.com/mozilla/geckodriver/releases

# Baixar o Selenium Standalone Server:
# https://selenium-release.storage.googleapis.com/index.html

# Rodar o Selenium e o GeckoDriver em um terminal:
# java -jar -Dwebdriver.gecko.driver="/home/livre/Downloads/geckodriver-v0.31.0-linux64/geckodriver" Downloads/selenium-server-standalone-3.9.1.jar

# Seguir os passos em Selenium Basics:
# https://docs.ropensci.org/RSelenium/articles/basics.html


# Faz login na API a partir dos dados de um arquivo com login e senha
fazer_login_api <- function(txt_file_number = '1') {

  # Conectar a um servidor Selenium
  remDr <- remoteDriver(
    remoteServerAddr = "localhost",
    port = 4444L,
    browserName = "firefox"
  )

  # Abrir o navegador:
  remDr$open()

  # Ir para a página da API de radares:
  remDr$navigate("http://dadosradares.prefeitura.sp.gov.br/login/")
  # remDr$getCurrentUrl()

  # Fazer login no site com user, tab, pass, enter
  api_txt_file <- sprintf('../../api_radares%s.txt', txt_file_number)
  my_api <- data.table::fread(api_txt_file, header = FALSE)

  webElem <- remDr$findElement(using = "class", "w3-input")
  webElem$sendKeysToElement(list(my_api$V1, "\ue004", my_api$V2, "\uE007"))
}




# # Tutorial de navegação nas páginas do https://docs.ropensci.org/RSelenium/index.html
# remDr$navigate("https://cran.r-project.org/")
# remDr$screenshot(display = TRUE)
#
# # Encontrar os três frames da página
# webElems <- remDr$findElements(using = "tag name", "frame")
# sapply(webElems, function(x){x$getElementAttribute("src")})
#
# # Entrar no frame "banner"
# remDr$switchToFrame(webElems[[3]])
# XML::htmlParse(remDr$getPageSource()[[1]])
#
# # Pegar links de download do R dentro deste frame
# webElems <- remDr$findElements("partial link text", "Download R")
# sapply(webElems, function(x) x$getElementText())
#
# # Buscar localização na tela do primeiro link
# loc <- webElems[[1]]$getElementLocation()
# loc[c('x','y')]
#
# # Mover o mouse para a localização encontrada
# remDr$mouseMoveToLocation(webElement = webElems[[1]])
#
# # Clicar com o direito e abrir em uma nova janela (não funciona o clique com o direito)
# remDr$click(2) # 2 indicates click the right mouse button
# remDr$sendKeysToActiveElement(
#   list(key = 'down_arrow', key = 'down_arrow', key = 'enter')
# )
#
# # Pegar identificadores da nova janela e de todas as janelas
# remDr$getCurrentWindowHandle()
# remDr$getWindowHandles()
# remDr$getTitle()
#
#
# currWin <- remDr$getCurrentWindowHandle()
# allWins <- unlist(remDr$getWindowHandles())
# otherWindow <- allWins[!allWins %in% currWin[[1]]]
# remDr$switchToWindow(otherWindow)
# remDr$getTitle()






# https://thatdatatho.com/tutorial-web-scraping-rselenium/
# https://stackoverflow.com/questions/15570598/saving-page-content-using-selenium
